//
//  Article.h
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/6/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString *const kArticleDeletedNotification;

@interface Article : NSManagedObject

- (void)copyFromJSON:(NSDictionary *)json;
+ (NSArray *)activeArticles;
- (void)autoInactive;

@end

NS_ASSUME_NONNULL_END

#import "Article+CoreDataProperties.h"
