//
//  Article+CoreDataProperties.m
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/6/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Article+CoreDataProperties.h"

@implementation Article (CoreDataProperties)

@dynamic articleId;
@dynamic name;
@dynamic subtitle;
@dynamic imageUrl;
@dynamic deleted;
@dynamic firstLetter;

@end
