//
//  Article.m
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/6/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import <MagicalRecord/MagicalRecord.h>
#import "Article.h"
#import "NSDictionary+Helper.h"

NSString *const kArticleDeletedNotification = @"articleDeleted";

@implementation Article

#pragma mark - Public

- (void)copyFromJSON:(NSDictionary *)json {
    self.articleId = [json notNullValueForKey:@"articleId"];
    self.name = [json notNullValueForKey:@"articleTitle"];
    self.subtitle = [json notNullValueForKey:@"articleSubTitle"];
    self.imageUrl = [json notNullValueForKey:@"articleImage"];
    self.firstLetter = [[self.name uppercaseString] substringToIndex:1];
    if (!self.deleted) {
        self.deleted = @NO;
    }
}

+ (NSArray *)activeArticles {
    NSArray *articles = [self MR_findAllSortedBy:@"name" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"deleted == NO"]];
    return articles;
}

- (void)autoInactive {
    self.deleted = @YES;
    [self.managedObjectContext MR_saveToPersistentStoreAndWait];
    [[NSNotificationCenter defaultCenter] postNotificationName:kArticleDeletedNotification object:self];
}

#pragma mark - Override

- (NSString *)description {
    return self.name;
}

@end
