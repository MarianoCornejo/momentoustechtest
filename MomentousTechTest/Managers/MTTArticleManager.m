//
//  MTTArticleManager.m
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/9/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import "MTTArticleManager.h"
#import "MTTArticleService.h"
#import "Article.h"

@implementation MTTArticleManager

#pragma mark - Singleton

+ (instancetype)sharedManager {
    static id __strong sharedObject = nil;
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

#pragma mark - Custom accesors

- (NSArray *)articles {
    _articles = [Article activeArticles];
    return _articles;
}

#pragma mark - Public

- (void)loadArticlesSuccess:(void (^)(NSArray *articles))success
                    failure:(void (^)(NSError *error))failure {
    [[MTTArticleService sharedService] retrieveArticlesSuccess:^(NSArray *articles) {
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
            for (NSDictionary *json in articles) {
                Article *article = [Article MR_findFirstOrCreateByAttribute:@"articleId" withValue:json[@"articleId"] inContext:localContext];
                [article copyFromJSON:json];
            }
        } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
            if (!error) {
                success(self.articles);
            }
        }];
    } failure:^(NSError *error) {
        failure(error);
    }];
}

@end
