//
//  MTTArticleManager.h
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/9/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTTArticleManager : NSObject

@property (strong, nonatomic) NSArray *articles;

+ (instancetype)sharedManager;

- (void)loadArticlesSuccess:(void(^)(NSArray *articles))success
                    failure:(void(^)(NSError *error))failure;

@end
