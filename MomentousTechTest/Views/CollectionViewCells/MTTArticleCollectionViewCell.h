//
//  MTTArticleCollectionViewCell.h
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/9/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

@class Article;

@interface MTTArticleCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *articleImageView;
@property (weak, nonatomic) IBOutlet UILabel *articleNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *articleSubtitleLabel;

- (void)setUpCell:(Article *)article;

@end
