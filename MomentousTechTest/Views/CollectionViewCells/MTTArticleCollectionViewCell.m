//
//  MTTArticleCollectionViewCell.m
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/9/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import "MTTArticleCollectionViewCell.h"
#import "Article.h"

@implementation MTTArticleCollectionViewCell

#pragma mark - Override

- (void)awakeFromNib {
    CGFloat labelWidth = ([UIScreen mainScreen].bounds.size.width > [UIScreen mainScreen].bounds.size.height) ? [UIScreen mainScreen].bounds.size.height : [UIScreen mainScreen].bounds.size.width;
    self.articleNameLabel.preferredMaxLayoutWidth = labelWidth - 16;
    self.articleSubtitleLabel.preferredMaxLayoutWidth = labelWidth -16;
}

#pragma mark - Private

- (void)setUpFont {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.articleNameLabel.font = [UIFont boldSystemFontOfSize:defaults.titleFontSize.integerValue];
    self.articleSubtitleLabel.font = [UIFont systemFontOfSize:defaults.subtitleFontSize.integerValue];
}

#pragma mark - Pubic

- (void)setUpCell:(Article *)article {
    [self setUpFont];
    [self.articleImageView sd_setImageWithURL:[NSURL URLWithString:article.imageUrl]];
    self.articleNameLabel.text = article.name;
    self.articleSubtitleLabel.text = article.subtitle;
}

@end
