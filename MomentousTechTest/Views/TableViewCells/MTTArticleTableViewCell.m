//
//  MTTArticleTableViewCell.m
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/9/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import "MTTArticleTableViewCell.h"
#import "Article.h"

@implementation MTTArticleTableViewCell

#pragma mark - Private

- (void)setUpFont {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.articleNameLabel.font = [UIFont boldSystemFontOfSize:defaults.titleFontSize.integerValue];
    self.articleSubtitleLabel.font = [UIFont systemFontOfSize:defaults.subtitleFontSize.integerValue];
}

#pragma mark - Pubic

- (void)setUpCell:(Article *)article {
    [self setUpFont];
    [self.articleImageView sd_setImageWithURL:[NSURL URLWithString:article.imageUrl]];
    self.articleNameLabel.text = article.name;
    self.articleSubtitleLabel.text = article.subtitle;
}

@end
