//
//  NSUserDefaults+Font.h
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/11/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (Font)

@property (readonly,nonatomic) NSNumber *titleFontSize,*subtitleFontSize;

@end
