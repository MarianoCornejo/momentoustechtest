//
//  NSDictionary+Helper.m
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/9/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import "NSDictionary+Helper.h"

@implementation NSDictionary (Helper)

#pragma mark - Public

- (id)notNullValueForKey:(NSString *)key {
    id value = self[key];
    if ([value isKindOfClass:[NSNull class]]) {
        return nil;
    }
    return value;
}

@end
