//
//  UIViewController+SVProgressHUD.m
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/6/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import "UIViewController+SVProgressHUD.h"
#import <SVProgressHUD/SVProgressHUD.h>

@implementation UIViewController (SVProgressHUD)

- (void)showActivityIndicator {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD show];
}

- (void)hideActivityIndicator {
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}

@end
