//
//  UIViewController+SVProgressHUD.h
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/6/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (SVProgressHUD)

- (void)showActivityIndicator;
- (void)hideActivityIndicator;

@end
