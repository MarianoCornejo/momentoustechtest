//
//  UIViewController+Alert.h
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/9/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Alert)

- (void)showOkCancelAlertViewWithTitle:(NSString *)title
                               message:(NSString *)message
                            completion:(void(^)(BOOL isOk))completion;
- (void)showErrorAlertViewWithTitle:(NSString *)title
                            message:(NSString *)message
                         completion:(void(^)(void))completion;

@end
