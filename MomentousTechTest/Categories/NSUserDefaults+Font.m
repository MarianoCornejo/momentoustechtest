//
//  NSUserDefaults+Font.m
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/11/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import "NSUserDefaults+Font.h"

@implementation NSUserDefaults (Font)

#pragma mark - Custom accesors

- (NSNumber *)titleFontSize {
    NSNumber *fontSize = [self objectForKey:@"titleFontSize"];
    if (fontSize) {
        return fontSize;
    }
    return @17;
}

- (NSNumber *)subtitleFontSize {
    NSNumber *fontSize = [self objectForKey:@"subtitleFontSize"];
    if (fontSize) {
        return fontSize;
    }
    return @15;
}

@end
