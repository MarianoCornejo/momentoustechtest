//
//  NSDictionary+Helper.h
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/9/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Helper)

- (id)notNullValueForKey:(NSString *)key;

@end
