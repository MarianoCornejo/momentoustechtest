//
//  main.m
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/6/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
