//
//  MTTArticleListViewController.h
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/9/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import "MTTArticleDetailViewController.h"

@interface MTTArticleListViewController : MTTBaseViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultController;

@end
