//
//  MTTArticleDetailViewController.m
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/9/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import "MTTArticleDetailViewController.h"
#import "Article.h"

NSString *const kAlertTitle = @"";
NSString *const kAlertMessage = @"Are you sure you want to delete this article?";

@implementation MTTArticleDetailViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
}

#pragma mark - Override

- (void)defaultsChanged:(NSNotification *)notification {
    [self setUpFont];
}

#pragma mark - Private

- (void)setUpFont {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.articleNameLabel.font = [UIFont boldSystemFontOfSize:defaults.titleFontSize.integerValue];
    self.articleSubtitleLabel.font = [UIFont systemFontOfSize:defaults.subtitleFontSize.integerValue];
    [self.view layoutIfNeeded];
}

- (void)setUpView {
    [self setUpFont];
    [self.articleImageView sd_setImageWithURL:[NSURL URLWithString:self.selectedArticle.imageUrl]];
    self.articleNameLabel.text = self.selectedArticle.name;
    self.articleSubtitleLabel.text = self.selectedArticle.subtitle;
}

#pragma mark - Actions

- (IBAction)deleteArticle:(id)sender {
    [self showOkCancelAlertViewWithTitle:kAlertTitle message:kAlertMessage completion:^(BOOL isOk) {
        if (isOk) {
            [self.selectedArticle autoInactive];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (IBAction)hideImageView:(id)sender {
    self.buttonHeightConstraint.constant = 0;
    self.titleLabelVerticalSpacingConstraint.constant = 0;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
}
@end
