//
//  MTTArticleCollectionViewController.m
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/9/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import "MTTArticleCollectionViewController.h"
#import "MTTArticleDetailViewController.h"
#import "MTTArticleCollectionViewCell.h"
#import "MTTArticleManager.h"
#import "Article.h"

NSString *const kErrorAlertTitle2 = @"";
NSString *const kShowDetailSegueIdentifier2 = @"showDetailSegue";
NSString *const kArticleCellIdentifier2 = @"MTTArticleCollectionViewCell";

@interface MTTArticleCollectionViewController ()
@property (strong, nonatomic) NSMutableArray *articles;
@property (strong, nonatomic) MTTArticleManager *articleManager;
@property (strong, nonatomic) Article *selectedArticle;
@end

@implementation MTTArticleCollectionViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    [self loadArticles];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletedArticleNotification:) name:kArticleDeletedNotification object:nil];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    MTTArticleDetailViewController *destinationVC = segue.destinationViewController;
    destinationVC.selectedArticle = self.selectedArticle;
}

#pragma mark - Custom accesors

- (MTTArticleManager *)articleManager {
    if (!_articleManager) {
        _articleManager = [MTTArticleManager sharedManager];
    }
    return _articleManager;
}

#pragma mark - Override

- (void)defaultsChanged:(NSNotification *)notification {
    [self.collectionView reloadData];
}

#pragma mark - Private

- (void)setUpView {
    ((UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout).estimatedItemSize = CGSizeMake(1,1);
    self.articles = [self.articleManager.articles mutableCopy];
}

- (void)loadArticles {
    [self showActivityIndicator];
    [self.articleManager loadArticlesSuccess:^(NSArray *articles) {
        [self hideActivityIndicator];
        self.articles = [articles mutableCopy];
        [self.collectionView reloadData];
    } failure:^(NSError *error) {
        [self hideActivityIndicator];
        [self showErrorAlertViewWithTitle:kErrorAlertTitle2 message:error.localizedDescription completion:nil];
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.articles.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MTTArticleCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kArticleCellIdentifier2 forIndexPath:indexPath];
    [cell setUpCell:self.articles[indexPath.row]];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    self.selectedArticle = self.articles[indexPath.row];
    [self performSegueWithIdentifier:kShowDetailSegueIdentifier2 sender:nil];
}

#pragma mark - Notifications

- (void)deletedArticleNotification:(NSNotification *)notification {
    Article *article = notification.object;
    //remove from datasource
    [self.articles removeObject:article];
    [self.collectionView reloadData];
}

@end
