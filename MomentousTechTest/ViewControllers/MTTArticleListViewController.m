//
//  MTTArticleListViewController.m
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/9/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import "UIViewController+SVProgressHUD.h"
#import "UIViewController+Alert.h"
#import "MTTArticleListViewController.h"
#import "MTTArticleDetailViewController.h"
#import "MTTArticleTableViewCell.h"
#import "MTTArticleManager.h"
#import "Article.h"

NSString *const kErrorAlertTitle = @"";
NSString *const kShowDetailSegueIdentifier = @"showDetailSegue";
NSString *const kArticleCellIdentifier = @"MTTArticleTableViewCell";
NSInteger const kEstimatedRowHeight = 122;

@interface MTTArticleListViewController ()
@property (strong, nonatomic) MTTArticleManager *articleManager;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) Article *selectedArticle;
@end

@implementation MTTArticleListViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    [self loadArticles];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletedArticleNotification:) name:kArticleDeletedNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.selectedIndexPath = nil;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    MTTArticleDetailViewController *destinationVC = segue.destinationViewController;
    destinationVC.selectedArticle = self.selectedArticle;
}

#pragma mark - Custom accesors

- (MTTArticleManager *)articleManager {
    if (!_articleManager) {
        _articleManager = [MTTArticleManager sharedManager];
    }
    return _articleManager;
}

- (NSFetchedResultsController *)fetchedResultController {
    if (_fetchedResultController != nil) {
        return _fetchedResultController;
    }
    
    _fetchedResultController = [Article MR_fetchAllSortedBy:@"name" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"deleted == NO"] groupBy:@"firstLetter" delegate:self];
    return _fetchedResultController;
}

#pragma mark - Override

- (void)defaultsChanged:(NSNotification *)notification {
    [self.tableView reloadData];
}

#pragma mark - Private

- (void)setUpView {
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = kEstimatedRowHeight;
    [self.tableView reloadData];
}

- (void)loadArticles {
    [self showActivityIndicator];
    [self.articleManager loadArticlesSuccess:^(NSArray *articles) {
        [self hideActivityIndicator];
        NSError *error;
        if (![self.fetchedResultController performFetch:&error]) {
            // Update to handle the error appropriately.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }

        [self.tableView reloadData];
    } failure:^(NSError *error) {
        [self hideActivityIndicator];
        [self showErrorAlertViewWithTitle:kErrorAlertTitle message:error.localizedDescription completion:nil];
    }];
}

- (void)deleteArticleAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedIndexPath = indexPath;
    //put as inactive in coredata
    Article *article = [self.fetchedResultController objectAtIndexPath:self.selectedIndexPath];
    [article autoInactive];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id sectionInfo = [[self.fetchedResultController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MTTArticleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kArticleCellIdentifier forIndexPath:indexPath];
    Article *article = [self.fetchedResultController objectAtIndexPath:indexPath];
    [cell setUpCell:article];
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self showOkCancelAlertViewWithTitle:kAlertTitle message:kAlertMessage completion:^(BOOL isOk) {
            if (isOk) {
                [self deleteArticleAtIndexPath:indexPath];
            } else {
                [self.tableView setEditing:NO animated:YES];
            }
        }];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultController sections] objectAtIndex:section];
    return [sectionInfo indexTitle];
}

- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return [self.fetchedResultController sectionIndexTitles];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return [self.fetchedResultController sectionForSectionIndexTitle:title atIndex:index];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.selectedIndexPath = indexPath;
    self.selectedArticle = [self.fetchedResultController objectAtIndexPath:self.selectedIndexPath];
    [self performSegueWithIdentifier:kShowDetailSegueIdentifier sender:nil];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([searchText length] == 0) {
        self.fetchedResultController = nil;
    } else {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"deleted == NO AND name contains[cd] %@", searchText];
        [[self.fetchedResultController fetchRequest] setPredicate:predicate];
    }
    
    NSError *error;
    if (![self.fetchedResultController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    [self.tableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text = @"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    
    self.fetchedResultController = nil;
    NSError *error;
    if (![self.fetchedResultController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    [self.tableView reloadData];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
}

#pragma mark - NSFetchedResultControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeUpdate: {
            MTTArticleTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            Article *article = [self.fetchedResultController objectAtIndexPath:indexPath];
            [cell setUpCell:article];
            break;
        }
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark - Notifications

- (void)deletedArticleNotification:(NSNotification *)notification {
    self.fetchedResultController = nil;
    self.selectedIndexPath = nil;
    NSError *error;
    if (![self.fetchedResultController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    [self.tableView reloadData];
}

@end
