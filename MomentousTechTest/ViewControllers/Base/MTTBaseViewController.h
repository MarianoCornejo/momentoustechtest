//
//  MTTBaseViewController.h
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/10/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTTBaseViewController : UIViewController

- (void)defaultsChanged:(NSNotification *)notification;

@end
