//
//  MTTBaseViewController.m
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/10/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import "MTTBaseViewController.h"

@interface MTTBaseViewController ()

@end

@implementation MTTBaseViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(defaultsChanged:)
                                                 name:NSUserDefaultsDidChangeNotification
                                               object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Protected

- (void)defaultsChanged:(NSNotification *)notification {
    //do nothing
}

@end
