//
//  MTTArticleDetailViewController.h
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/9/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import "MTTBaseViewController.h"

@class Article;

extern NSString *const kAlertTitle;
extern NSString *const kAlertMessage;

@interface MTTArticleDetailViewController : MTTBaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *articleImageView;
@property (weak, nonatomic) IBOutlet UILabel *articleNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *articleSubtitleLabel;
@property (strong, nonatomic) Article *selectedArticle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelVerticalSpacingConstraint;

- (IBAction)hideImageView:(id)sender;

@end
