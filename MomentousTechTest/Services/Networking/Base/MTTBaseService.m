//
//  MTTBaseService.m
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/6/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import "MTTBaseService.h"

NSString *const kBaseUrl = @"http://www.json-generator.com/api/json/get";

@implementation MTTBaseService

#pragma mark - Custom accesors

- (NSString *)baseUrl {
    if (!_baseUrl) {
        _baseUrl = kBaseUrl;
    }
    return _baseUrl;
}

#pragma mark - Private

- (AFHTTPRequestOperationManager *)manager {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    return manager;
}

#pragma mark - Public

- (void)GET:(NSString *)serviceName
 parameters:(id)parameters
    success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
    
    NSString *url = [NSString stringWithFormat:@"%@/%@",self.baseUrl,serviceName];
    
    AFHTTPRequestOperationManager *manager = [self manager];
    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation,responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation,error);
    }];
}

@end
