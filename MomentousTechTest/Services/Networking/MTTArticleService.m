//
//  MTTArticleService.m
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/6/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import "MTTArticleService.h"

@implementation MTTArticleService

#pragma mark - Singleton

+ (instancetype)sharedService {
    static dispatch_once_t onceToken = 0;
    static id __strong sharedObject = nil;
    dispatch_once(&onceToken, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

#pragma mark - Public

- (void)retrieveArticlesSuccess:(void(^)(NSArray *articles))success
                        failure:(void(^)(NSError *error))failure {
    [self GET:@"bYKqOKXiEO?indent=3" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

@end
