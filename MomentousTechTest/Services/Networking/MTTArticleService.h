//
//  MTTArticleService.h
//  MomentousTechTest
//
//  Created by Oscar Mariano Cornejo Herrera on 7/6/16.
//  Copyright © 2016 PL Develop. All rights reserved.
//

#import "MTTBaseService.h"

@interface MTTArticleService : MTTBaseService

+ (instancetype)sharedService;

- (void)retrieveArticlesSuccess:(void(^)(NSArray *articles))success
                        failure:(void(^)(NSError *error))failure;

@end
