# MomentousTechTest by Oscar Mariano Cornejo Herrera

In the next lines im going to explan the project architecture.

The main pattern used for this project was MVC, is good to know this Article Model is not a dummy model , is a smart model tightly coupled with  Core Data, and allows to do some core data operations, instead of be a simple data holder.
Also as a part of the model there is a Manager class that allows and simplifies syncronization between core data and the service layer, this service layer is used to perform network requests.
Inspired in onion architecture there is also a Infraestructure layer conformed by categories, views, resources, supporting files, etc.

# Folders distribution

  - AppDelegate
  - Categories
  - Managers
  - Model
  - Resources
  - Services
  - Storyboards
  - Supporting files
  - ViewControllers
  - Views

# Some considerations

- As a dependency manager i´m using the last version of cocoapods, dont forget use sudo gem install cocoapods in order to get the last version
- Groups are real folders not logical, keep in that way
- Don´t put the code from develop to master until QA aproved
- Don´t forget open the workspace instead of the xcode project
- Have a nice day :)